from odoo import models, fields, api

class PesananWizard(models.TransientModel):
    _name = 'hmcoffee.pesanan.wizard'
    _description = 'Pesanan Wizard'

    # Define your wizard fields here
    dari = fields.Date(string='Dari Tanggal')
    ke = fields.Date(string='Sampai Tanggal')
    pesanan_id = fields.Many2one('hmcoffee.pesanan', string='Pesanan')

    def filter_pesanan(self):
        # Perform filtering logic here
        filtered_pesanan = self.env['hmcoffee.pesanan'].search_read([
            ('tanggal_pesanan', '>=', self.dari) if self.dari else [],
            ('tanggal_pesanan', '<=', self.ke) if self.ke else [],
        ])
        print(filtered_pesanan)
        data={
            'form':self.read()[0],
            'laporannya':filtered_pesanan
        }
        report_action = self.env.ref('hmcoffee.record_pesanan_wizard_pdf').report_action(self,data=data)
        report_action['close_on_report_download'] = True
        return report_action